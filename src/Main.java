import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Highway highway = new Highway();
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz komende, aby wyswietlic liste komend wpisz 'lista'.");
        String commandUntrimmed = sc.nextLine();
        String command[] = commandUntrimmed.toLowerCase().split(" ");

        do{
            if (command[0].equals("quit")){break;}
            else if (command[0].equals("wjazd")){
                String registrationNumber = command[1];
                System.out.println("Wpisz typ samochodu (TRUCK, CAR, MOTORCYCLE).");
                CarType type = CarType.valueOf(sc.nextLine().toUpperCase());
                highway.vehicleEntry(registrationNumber, type);
                System.out.println("Samochod wjechal na autostrade.");
                System.out.println("Wpisz komende, aby wyswietlic liste komend wpisz 'lista'.");
            }
            else if (command[0].equals("wyjazd")){
                String registrationNumber = command[1];
                highway.vehicleLeave(registrationNumber);
                System.out.println("Samochod wyjechal z autostrady.");
                System.out.println("Wpisz komende, aby wyswietlic liste komend wpisz 'lista'.");
            }
            else if (command[0].equals("sprawdz")){
                String registrationNumber = command[1];
                highway.searchVehicle(registrationNumber);
                System.out.println("Wpisz komende, aby wyswietlic liste komend wpisz 'lista'.");
            }
            else if (command[0].equals("lista")){
                System.out.println("wjazd, wyjazd, sprawdz, lista, quit.");
            }
            else{
                System.out.println("Wpisales zla komende.");
                System.out.println("Wpisz komende, aby wyswietlic liste komend wpisz 'lista'.");
            }
            commandUntrimmed = sc.nextLine();
            command = commandUntrimmed.toLowerCase().split(" ");
        } while (!command[0].equals("quit"));
    }
}

//  Stwórz main który przyjmuje komendy
//        - wjazd NR_REJESTRACYJNY TYP
//        - wyjazd NR_REJESTRACYJNY
//        - sprawdz NR_REJESTRACYJNY
//        i wykonuje odpowiednie akcje na instancji klasy highway.
