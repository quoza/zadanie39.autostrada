import java.time.LocalDateTime;

public class VehicleInfo {
    private String registrationNumber;
    private CarType carType;
    private LocalDateTime timeOfEntrance;

    public VehicleInfo(String registrationNumber, CarType carType, LocalDateTime timeOfEntrance) {
        this.registrationNumber = registrationNumber;
        this.carType = carType;
        this.timeOfEntrance = timeOfEntrance;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public LocalDateTime getTimeOfEntrance() {
        return timeOfEntrance;
    }

    public void setTimeOfEntrance(LocalDateTime timeOfEntrance) {
        this.timeOfEntrance = timeOfEntrance;
    }

    @Override
    public String toString() {
        return "VehicleInfo{" +
                "registrationNumber='" + registrationNumber + '\'' +
                ", carType=" + carType +
                ", timeOfEntrance=" + timeOfEntrance +
                '}';
    }
}
