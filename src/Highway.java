import java.time.Duration;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

public class Highway {
    private List<VehicleInfo> vehicleList = new LinkedList<>();

    public void vehicleEntry(String registrationNumber, CarType type){
        LocalDateTime timeOfEntrance = LocalDateTime.now();
        vehicleList.add(new VehicleInfo(registrationNumber,type,timeOfEntrance));
    }

    public void searchVehicle(String registrationNumber){
        boolean flag = false;
        for (VehicleInfo vehicle : vehicleList){
            if (vehicle.getRegistrationNumber().equals(registrationNumber)){
                System.out.println("Samochod znajduje sie na autostradzie.");
                flag = true;
            }
        }
        if (!flag){
            System.out.println("Nie znaleziono samochodu.");
        }
    }

    public void vehicleLeave(String registrationNumber) throws Exception {
        int counter = 0;
        for (VehicleInfo vehicle : vehicleList){
            if (vehicle.getRegistrationNumber().equals(registrationNumber)){
                vehicleList.remove(vehicle);
                System.out.println("Usunieto pojazd o numerze rejestracyjnym " + registrationNumber + ".");
                counter++;
                LocalDateTime ldt1 = vehicle.getTimeOfEntrance();
                LocalDateTime ldt2 = LocalDateTime.now();
                long duration = Duration.between(ldt1, ldt2).getSeconds();
                long amountToPay = duration;
                System.out.println("Do zaplaty: " + amountToPay + " zl.");
            }
        }
        if (counter == 0){
            throw new Exception("Nie znaleziono samochodu o podanym numerze rejestracyjnym.");
        }
    }
}